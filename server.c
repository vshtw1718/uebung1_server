#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include<unistd.h>

#include "server.h"

int main(int argc, char *argv[]) {

    //Settings
    int opt, port, queueSize, ret;

    while ((opt = getopt(argc, argv, "pq:")) != -1) {
        switch (opt) {
            case 'p':
                port = atoi(argv[optind]);
                break;
            case 'q':
                queueSize = atoi(optarg);
                break;
            default:
                printf("usage: server -p portnumber -q queuesize", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    ret = startServer(port,queueSize);
    return ret;

}

/**
 * start server and listen to incoming requests
 * @return
 */
int startServer(int port, int queueSize) {

    int socketDescriptor, clientSocketConnection, structSize, *clientSocket;
    struct sockaddr_in server, client;

    //create socket
    socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socketDescriptor == -1) {
        printf("Creating a socket failed");
    }
    puts("Socket created");

    //set socket settings
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(port);

    //bind socket to port
    if (bind(socketDescriptor, (struct sockaddr *) &server, sizeof(server)) < 0) {
        perror("Can't bind server to socket");
        return 1;
    }
    puts("Bind success");

    //listen on messages from clients
    if (listen(socketDescriptor, queueSize) == -1) {
        perror("Listen failed");
        return 2;
    }

    puts("Waiting for requests");
    structSize = sizeof(struct sockaddr_in);

    //read incoming requests
    while ((clientSocketConnection = accept(socketDescriptor, (struct sockaddr *) &client,
                                            (socklen_t *) &structSize))) {
        if (clientSocketConnection < 0) {
            perror("Cant connect to client");
            return 3;
        }
        puts("Connected to client socket");

        //save client information in new pointer for thread
        pthread_t workThread;
        clientSocket = malloc(1);
        *clientSocket = clientSocketConnection;

        //create new thread for request
        if (pthread_create(&workThread, NULL, getBytesFromFiles, (void *) clientSocket) < 0) {
            perror("create new thread failed");
            return 1;
        }

        pthread_join(workThread, NULL);
        puts("new thread created");

        if (clientSocket < 0) {
            perror("accept failed");
            return 1;
        }

        // return 0;

    }
}

// thread: search fileinfo and return it to the client

void *getBytesFromFiles(void *socketDescriptor) {

    int sock = *(int *) socketDescriptor;
    int readSize, byteSize = 0, i = 0 , fullSize =0;
    char client_message[2000];
    char *params[6] = {}, *values[6] = {};
    char *reply, *bytes;

    readSize = recv(sock, client_message, 2000, 0);

    if (readSize > 0) {
        printf("Request: %s\n", client_message);
        int count = 0;

        for (int i = 0; client_message[i] != '\0'; i++) {
            if (client_message[i] == '&')
                count++;
        }


        char *p = strtok(client_message, "&");

        while (p != NULL) {
            params[i++] = p;
            p = strtok(NULL, "&");
        }


        for (i = 0; i <= count; i++) {
            values[i] = strstr(params[i], "=");
            values[i] = ++values[i];
        }

        byteSize = atoi(values[0]);

        for (int j = 1; j <= count; j++) {
            fullSize += strlen(values[j]);
        }


        fullSize += (count + 1);


        char * (*tmpValues);

        for(i = 1; i <= count; i++) {
            tmpValues = readFile(values[i],byteSize);
            fullSize += sizeof(tmpValues);
        }

        reply = calloc(fullSize,sizeof(char));

        for (i = 1; i <= count; i++) {
            strcat(reply, values[i]);
            strcat(reply, "=");

            printf("Read file %s\n", values[i]);
            bytes = readFile(values[i], byteSize);
            strcat(reply, bytes);

            if (i != count) {
                strcat(reply, "&");
            }
            if(bytes != "File not Found") {
                free(bytes);
            }

        }

        printf("Send Reply: %s\n", reply);
        write(sock, reply, strlen(reply));
    }


    if (readSize == 0) {
        puts("Client disconnected");
        fflush(stdout);
    } else if (readSize == -1) {
        perror("Recv failed");
    }

    //Free the socket pointer
    free(socketDescriptor);

    return 0;

}

/**
 * read bytes of a file
 * @param filename file to read
 * @param byteSize number of bytes to read
 * @return
 */
char *readFile(char *filename, int byteSize) {
    char *bytes, *notFound = "File not found";
    FILE *pFile;

    pFile = fopen(filename, "r");
    //TODO MSZ wenn file nicht gefunden eine meldung anfügen kein fehler
    if (pFile == NULL) {
        bytes = (char *) malloc(sizeof(*notFound));
        strcpy(bytes,notFound);
        return bytes;
    }

    // allocate memory to contain the whole file:
    bytes = (char *) calloc(byteSize,sizeof(char));

    fread(bytes, byteSize, 1, pFile);
    fclose(pFile);

    return bytes;
}

