//
// Created by Marc Szymkowiak
//

#ifndef SERVER_SERVER_H
#define SERVER_SERVER_H

void *getBytesFromFiles(void *);
int startServer(int,int);
char* readFile(char*, int);


#endif //SERVER_SERVER_H
